// Scope all code so that we can control what seeps into the window object
(function () {
    var PG_SessionManager_JS = function() {
    };

    /**
     * X2O Object properties are typed, and a few components of the system are sensitive to this type (Channel Designer's property list dialog
     * will display a specific input field for certain types, for example). Values not specified in this list will be tolerated, but have no
     * special effect in the system.
     *
     * Each value is defined here with its name followed by the input control shown by Channel Designer and notes if applicable, separated by "//";
     * names are case-insensitive.
     *
     * So although this documentation presents a data structure, this is in fact a list of all possible values (that also explains why all "properties"
     * below are presented with a type of {string}, regardless of the information they carry) for an X2O object's custom property type.
     *
     * @property {string} Audio         The identifier of an audio asset.   <br>A dialog that navigates the current network, restricted to audio assets.
     * @property {string} Bool          A value of "True" or "False".       <br> A drop-down with solely "True" or "False" as options.
     * @property {string} Boolean       A value of "True" or "False".       <br> A drop-down with solely "True" or "False" as options.
     * @property {string} Channel       A channel in the X2O Platform.      <br> A channel selector dialog to locate and select a channel.
     * @property {string} Color         Represents a color value (RGB).     <br> A color-picker, where the user can also set an RGB value manually.
     * @property {string} Colorlist     Represents a color value (RBG).     <br> A list of pre-defined colors to pick from doubled with a manual entry textbox.
     * @property {string} Contentfolder Path to a content folder.           <br> A treeview-based content folder browser/selector.
     * @property {string} Customeditor  Specialized text content.           <br> A dialog containing an editor as defined by the object. (See x2o.ObjectBase API documentation)
     * @property {string} DataDocument  The identifier of a known data document. <br> A dialog that allows navigating known data documents.
     * @property {string} Datafeed      The identifier of a known datafeed. <br> A dialog that allows navigating known datafeeds.
     * @property {string} Excel         The identifier of an Excel asset.   <br> A dialog that navigates the current network, restricted to Excel assets.
     * @property {string} File          The identifier of a file asset.     <br> A dialog that navigates the current network, restricted to file assets.
     * @property {string} Flash         The identifier of a flash asset.    <br> A dialog that navigates the current network, restricted to Flash assets.
     * @property {string} Flipbook      The identifier of a flipbook asset. <br> A dialog that navigates the current network, restricted to flipbook assets.
     * @property {string} Float         A floating point value.             <br> A textbox restricted to floating point number entry.
     * @property {string} Format        A temperature format indicator.     <br> A dropdown allowing for one of &deg;C or &deg;F or player setting.
     * @property {string} Font          A JSON-encoded font definition      <br> Although this field can be manually edited, it interacts with the font
     *                                                                           toolbar in Channel Designer; the object properties dialog only provides a
     *                                                                           raw text editor for this data.
     * @property {string} Image         The identifier of an image asset.   <br> A dialog that navigates the current network, restricted to image assets.
     * @property {string} Integer       An integer value.                   <br> A textbox restricted to numeric entry.
     * @property {string} Language      A language code identifier.         <br> A drop-down allowing selection from a few pre-defined languages, or player setting.
     * @property {string} Misc          The identifier of a misc asset.     <br> A dialog that navigates the current network, restricted to misc assets.
     * @property {string} Objectname    A valid object name.                <br> A textbox which respects Channel Designer object name restrictions.
     *
     * @property {string} Option{[values]} One of a set of pre-defined values.<br>
     *            A drop-down out of a possible values. <p>
     *            Values are to be supplied directly in the property typename, using hash keys as returned values and hash values as
     *            display text; {"1": "Today", "2": "Tomorrow"} would yield a drop-down with "Today" and "Tomorrow" as display entries
     *            and "1" and "2" as corresponding values. **It is highly recommended to supply this value as `'option' +
     *            JSON.stringify(object)`** as it guarantees proper JSON-encoding of the values.
     *
     * @property {string} Password      A string but displayed with asterisks.  <br> A textbox for freeform entry, that doesn't show the text.
     * @property {string} PDF           The identifier of a pdf asset.       <br> A dialog that navigates the current network, restricted to PDF assets.
     * @property {string} Playlist      An identifier to a playlist.         <br> A dialog tha navigates playlists for the current network.
     * @property {string} Powerpoint    The identifier of a PowerPoint asset.<br> A dialog that navigates the current network, restricted to PowerPoint assets.
     * @property {string} Richtext      Text value meant for rich content.<br> A dialog that offers an editor for rich text.
     * @property {string} Stretchmode   One of the possible stretchmodes.    <br> A drop-down with pre-defined values: keepaspect, stretch, originalsize.
     * @property {string} String        A string literal.                    <br> A textbox for freeform entry.
     * @property {string} Twitter       Possible Twitter operating modes.    <br> A drop-down with search modes by hash tag, by user account or freeform query.
     * @property {string} Video         The identifier of a video asset.     <br> A dialog that navigates the current network, restricted to video assets.
     * @property {string} Wayfinder     The identifier of a wayfinder asset. <br> A dialog that navigates the current network, restricted to wayfinder assets.
     */
    var objectProperties = [
        {
            name: "UserName",
            type: "string",
            description: "Enter the user name",
            extendedDescription: "User name is used to display the message \"Hello {{UserName}}\"",
            value: 'World',
            mainProperty: true
        },
        {
            name: "Color",
            type: "Color",
            description: "Font color",
            extendedDescription: "Color of the displayed text",
            value: '#ffffff'
        }
    ];

    // Inheritance setup
    PG_SessionManager_JS.inheritsFrom(x2o.ObjectBase);

    /**
     * This method is called when internal property values of the object have changed, as a result of:
     *  - Channel Designer's property dialog being used to modify properties at design-time
     *  - An object being brought to life at run-time
     *  - A data-bound property being updated automagically
     *  - Anytime an external entity calls `public.setProperties` on the object (see {@link x2o.ObjectBase~DefaultPublicInterface})
     *
     * @see x2o.ObjectBase#onPropertiesUpdated
     *
     * @param {string[]} updatedProps   A list containing the names of all properties affected by the operation
     * @param {string[]} updatedData    A list containing the names of all data-bound properties affected by the operation
     */
    PG_SessionManager_JS.prototype.onPropertiesUpdated = function (updatedProps, updated_data) {
        if(updatedProps.indexOf('UserName') > -1) {
            this.setUserName(this.properties_.values['UserName']);
        }

        if(updatedProps.indexOf('Color') > -1) {
            this.setColor(this.properties_.values['Color']);
        }
    };

    /**
     * Set text color for the message.
     *
     * @param color The new color value
     */
    PG_SessionManager_JS.prototype.setColor = function(color) {
        $('.message').css({
            color: color
        });
    };

    /**
     * Update the user name displayed
     *
     * @param name  The name to display
     */
    PG_SessionManager_JS.prototype.setUserName = function(name) {
        // Update displayed name
        $('#nameProperty').text(name);
    };

    /**
     * Object initialisation
     */
    PG_SessionManager_JS.prototype.init = function () {
        var self = this;
        var deferred = $.Deferred();
    
        /**
         * See Objects API documentation (Platform 
         */
        self.setInitOptions({
            showHeader: true,           // Default value
            title: {
                value: 'Title'          // Default value
            },
            backgroundStyle: 'object'   // Default value
        });
        
        $.when(this.construct(
            window.name,
            objectProperties
        )).then(function () {
            // Raises an event to signify that this object is ready to be used.
            // Only call this function once the object has finished initializing for 
            // the first time.
            // 
            // A channel will keep track of all existing object
            // and fire 'Channel:AllObjectsReady' when all objects have triggered this event.
            self.notifyObjectReady({
                name: PG_SessionManager_JS
            });

            deferred.resolve();
        });

        return deferred.promise();
    };

    // Export object from namespace
    window.PG_SessionManager_JS = new PG_SessionManager_JS();

    //Bootstrap
    $(function () {
        $.when(window.PG_SessionManager_JS.init()).then(function () {
            // POST-CONSTRUCTION CODE
            var app = $.sammy('#main', function () {
                //X2O vars
                var client = false,
                cartridgeId = 'f5ffdd31-7e9c-49c8-8934-26b4dbb749a6',
                restAPIURL = window.location.protocol + '//' + window.location.host + '/XmanagerWeb/rest/v1';
                epoxy_events_handler = {
                    connected: function () {
                        console.log("Epoxy client connected - client handler!");
                    },
                    disconnected: function() {
                        console.log("Epoxy client disconnected - client handler!");
                        client.unregisterClient(cartridgeId, epoxy_events_handler);
                    },
                    incoming: function (msg) {
                        console.log("Epoxy client data push received - client handler!");
                        app.trigger('x2o.epoxy.push', msg);
                    }
                };

                //USE - Sammy Templates
                this.use('Template');
                this.use('Storage');

                //SESSION STATE
                var pgcookie = new Sammy.Store({
                    name: 'pgsessionmanager',
                    type: 'cookie'
                });

                //INSTANCE STATE
                var pgstate = new Sammy.Store({
                    name: 'pgsessionmanager',
                    type: 'memory'
                });

                //INSTANCE tempstate
                var pgtempstate = new Sammy.Store({
                    name: 'pgsessiontempstate',
                    type: 'memory'
                });
                
                //Check logged in
                function checkLoggedIn (callback) {
                    //persist login from cookie
                    if(!pgstate.exists('current_user') && pgcookie.exists('current_user')) {
                        pgstate.set('current_user', pgcookie.get('current_user'));
                    }
                    //toggle state of logout button
                    if (pgstate.exists('current_user')) {
                        $('#app-logout').removeClass('d-none');
                    } else {
                        $('#app-logout').addClass('d-none');
                    }
                    callback();
                }

                //implement around of checkloggedin at every request
                this.around(checkLoggedIn);

                //ROUTE: #/, WEBROOT
                this.get('#/', function (context) {
                    if(pgstate.exists('current_user')) {
                        app.trigger('log.in');
                    }
                    context.partial('templates/login.template', {
                        item: {
                            message: 'Click \'Enter\' to proceed.'
                        }
                    });
                });
                
                //POST: #/login, Login POST
                this.post('#/login', function (context) {
                    var current_user = _.extend({
                        Username: 'TESTUSER'
                    }, context.params);
                    pgstate.set('current_user', current_user);
                    pgcookie.set('current_user', current_user);                    
                    app.trigger('log.in');
                    return false;
                })

                //GET: #/logout, Logout GET
                this.get('#/logout', function (context) {
                    app.trigger('log.out');
                    return false;
                });

                //UTILITY FUNCTIONS
                //get available areas from layout
                function getRecursiveAreasFromLayout (layout) {
                    return _.reduce(layout, function (acc, itm, idx) {
                        if ((itm.Children && (itm.Children.length > 0)) && (itm.Type !== 'Area')) {
                            acc.push(getRecursiveAreasFromLayout(itm.Children));
                        } else {
                            acc.push(itm);
                        }
                        return acc;
                    }, []);
                }

                function listAllAreasFromLayout (layout) {
                    return _.flatten(getRecursiveAreasFromLayout(layout));
                }

                function injectAreaDetails (area) {
                    var out = _.findWhere(pgstate.get('available_areas'), {
                        Id: area.Id
                    });
                    return _.extend({}, out, area);
                }

                function orderAreaList (areas) {
                    return _.compact(_.map(pgstate.get('available_areas'), function (itm, idx) {
                        return _.findWhere(areas, {
                            Id: itm.Id
                        });
                    }));
                }

                function getAreaData (area) {
                    return _.findWhere(pgstate.get('valid_selected_areas'), {
                        Id: area.Id
                    });
                }

                function isValidArea (area) {
                    return _.contains(_.pluck(pgstate.get('available_areas'), 'Id'), area.Id);
                }

                function isSelectedArea (area) {
                    return _.contains(_.pluck(pgstate.get('valid_selected_areas'), 'Id'), area.Id);
                }

                //INIT: Grid Click actions
                function initGridClicks () {
                    
                    $('.pg-action-btn').unbind('click').click(function (evt) {
                        var action = $(evt.currentTarget).data('actionId'),
                            sessionid = $(evt.currentTarget).data('sessionId'),
                            selected_session = _.findWhere(pgstate.get('sessions'), {
                                Id: sessionid
                            }), out = '';

                        switch (action) {
                            case 'view':

                                out = 
                                    '<div class="card" style="width: 100%; background: transparent;">'
                                    + '<div class="card-body">'
                                    + '<h5 class="card-title frutiger-bold font-24">Session: ' + selected_session.Name + '</h5>'
                                    + '<h6 class="card-subtitle mb-2 text-muted frutiger-bold font-22">Date: ' + selected_session.Date + '</h6>'
                                    + '<h6 class="card-subtitle mb-2 text-muted frutiger-bold font-22">Company: ' 
                                    + (selected_session.Company.length > 0 ? selected_session.Company : '---') 
                                    + '</h6>'
                                    + '<p class="card-text frutiger-light font-22">'
                                    + _.reduce(selected_session.Areas, function (acc, itm, idx) {
                                        // return acc + 'Area:' + itm.Name + ', Screens:' + itm.Screens.length + '<br>';
                                        var session_area = _.findWhere(pgstate.get('available_areas'), {
                                            Id:itm.Id 
                                        });
                                        return session_area ? acc + 'Area:' + session_area.Name + ', Screens:' + session_area.Children.length + '<br>' : acc;
                                    }, '')
                                    + '</p>'
                                    + '</div>'
                                    + '</div>';
                                //set modal content
                                $('#actionSessionModalContent').html(out);
                                //set modal title
                                $('#actionSessionModalLabel').text('Session details');

                                if(selected_session.IsActive) {
                                    //set modal button label
                                    $('#session-action').text('Load');
                                    //set modal button click action
                                    $('#session-action').unbind('click').click(function () {
                                        var toload = _.findWhere(pgstate.get('sessions'), {
                                            Id: sessionid
                                        });
                                        var areastoload = _.reduce(toload.Areas, function (acc, itm, idx) {
                                            var session_area = _.findWhere(pgstate.get('available_areas'), {
                                                Id:itm.Id 
                                            });
                                            var out = session_area ?  
                                                '<label class="pg-custom-checkbox custom-control custom-checkbox d-flex align-items-center justify-content-start">'
                                                + '<input type="checkbox" class="custom-control-input" data-area-id="' +  session_area.Id + '" checked>'
                                                + '<span class="custom-control-indicator mr-4"></span>'
                                                + '<span class="custom-control-description frutiger-light font-16">' + session_area.Name + '</span>'
                                                + '</label>' : '';
                                            return acc + out;
                                        }, '');
        
                                        //set modal content
                                        var out = '<p>Load Session \'' + toload.Name + '\' ?</p><p>Load Areas:</p>' + areastoload;
                                        $('#actionSessionModalContent').html(out);
                                        //set modal title
                                        $('#actionSessionModalLabel').text('Load session');
                                        //set modal button label
                                        $('#session-action').text('Proceed');
                                        //set modal button click action
                                        $('#session-action').unbind('click').click(function () {
                                            if($('#actionSessionModalContent input:checked').length !== toload.Areas.length) {
                                                var loadareas = _.map($('#actionSessionModalContent input:checked'), function (itm, idx) {
                                                    var areaid = $(itm).data('areaId');
                                                   return areaid;
                                                });
                                                _.each(loadareas, function (itm, idx) {
                                                    client.sendCommand({
                                                        'Id':cartridgeId
                                                    },{
                                                        "Event": "LOAD_AREA",
                                                        "Session": {
                                                            "Id": sessionid
                                                        },
                                                        "Area": {
                                                            "Id": itm
                                                        }
                                                    });
                                                })
                                            } else {
                                                client.sendCommand({
                                                    'Id':cartridgeId
                                                },{
                                                    "Event": "LOAD_SESSION",
                                                    "Session": {
                                                        Id: sessionid
                                                    }
                                                }).then(function () {
                                                    console.log('ALL AREAS LOADED');
                                                });    
                                            }
                                            $('#actionSessionModal').modal('hide');
                                        });
                                        $('#actionSessionModal').modal('show');
                                    });

                                } else {
                                    //set modal button label
                                    $('#session-action').text('Edit');
                                    //set modal button click action
                                    $('#session-action').unbind('click').click(function () {
                                        app.trigger('edit.session', {
                                            sessionid: sessionid
                                        });    
                                        $('#actionSessionModal').modal('hide');
                                    });
                                }

                                $('#actionSessionModal').modal('show');
                                break;
                            case 'edit':
                                app.trigger('edit.session', {
                                    sessionid: sessionid
                                });
                                break;
                            case 'clone':
                                var toclone = _.findWhere(pgstate.get('sessions'), {
                                    Id: sessionid
                                });
                                var clone_session = _.extend({}, toclone, {
                                    Name: 'Copy of ' + toclone.Name
                                });

                                //CLEAN CLONE
                                delete(clone_session.Id);
                                delete(clone_session.id);
                                delete(clone_session.CreatedBy);
                                delete(clone_session.Created);
                                delete(clone_session.Modified);
                                delete(clone_session.ModifiedBy);
                                delete(clone_session.IsActive);

                                //set modal content
                                $('#actionSessionModalContent').text('Clone Session \'' + toclone.Name + '\' ?');
                                //set modal title
                                $('#actionSessionModalLabel').text('Clone session');
                                //set modal button label
                                $('#session-action').text('Proceed');
                                //set modal button click action
                                $('#session-action').unbind('click').click(function () {
                                    client.sendCommand({
                                        'Id':cartridgeId
                                    },{
                                        "Event": "CREATE_SESSION",
                                        "Session": clone_session
                                    }).then(function (response) {
                                        var sessions = pgstate.get('sessions');
                                        var grid = $('#sessions-grid').data('JSGrid');
                                        if(response.success) {
                                            sessions.push(response.data);
                                            pgstate.set('sessions', sessions);
                                            grid.insertItem(response.data);
                                            grid.sort({
                                                field: 'IsActive',
                                                order: 'asc'
                                            });
                                        }
                                        $('#actionSessionModal').modal('hide');
                                    });
                                });
                                $('#actionSessionModal').modal('show');
                                break;
                            case 'delete':
                                var row_to_delete = $(evt.currentTarget).closest('tr');
                                var todelete = _.findWhere(pgstate.get('sessions'), {
                                    Id: sessionid
                                });

                                //set modal content
                                $('#actionSessionModalContent').text('Delete Session \'' + todelete.Name + '\' ?');
                                //set modal title
                                $('#actionSessionModalLabel').text('Delete session');
                                //set modal button label
                                $('#session-action').text('Proceed');
                                //set modal button click action
                                $('#session-action').unbind('click').click(function () {
                                    client.sendCommand({
                                        'Id':cartridgeId
                                    },{
                                        "Event": "REMOVE_SESSION",
                                        "Session": {
                                            Id: sessionid
                                        }
                                    }).then(function (response) {
                                        var grid = $('#sessions-grid').data('JSGrid');
                                        if(response.success) {
                                            pgstate.set('sessions', _.filter(pgstate.get('sessions'), function (itm, idx) {
                                                return itm.Id !== sessionid;
                                            }));
                                            grid.deleteItem(row_to_delete);
                                            grid.sort({
                                                field: 'IsActive',
                                                order: 'asc'
                                            });
                                        }
                                        $('#actionSessionModal').modal('hide');
                                    });
                                });
                                $('#actionSessionModal').modal('show');
                                break;

                            case 'load':
                                var toload = _.findWhere(pgstate.get('sessions'), {
                                    Id: sessionid
                                });

                                var areastoload = _.reduce(toload.Areas, function (acc, itm, idx) {
                                    var session_area = _.findWhere(pgstate.get('available_areas'), {
                                        Id:itm.Id 
                                    });
                                    var out = session_area ? 
                                        '<label class="pg-custom-checkbox custom-control custom-checkbox d-flex align-items-center justify-content-start">'
                                        + '<input type="checkbox" class="custom-control-input" data-area-id="' +  session_area.Id + '" checked>'
                                        + '<span class="custom-control-indicator mr-4"></span>'
                                        + '<span class="custom-control-description frutiger-light font-16">' + session_area.Name + '</span>'
                                        + '</label>' : '';
                                    return acc + out;
                                }, '');

                                //set modal content
                                var out = '<p>Load Session \'' + toload.Name + '\' ?</p><p>Load Areas:</p>' + areastoload;
                                $('#actionSessionModalContent').html(out);
                                //set modal title
                                $('#actionSessionModalLabel').text('Load session');
                                //set modal button label
                                $('#session-action').text('Proceed');
                                //set modal button click action
                                $('#session-action').unbind('click').click(function () {
                                    if($('#actionSessionModalContent input:checked').length !== toload.Areas.length) {
                                        var loadareas = _.map($('#actionSessionModalContent input:checked'), function (itm, idx) {
                                            var areaid = $(itm).data('areaId');
                                           return areaid;
                                        });

                                        _.each(loadareas, function (itm, idx) {
                                            client.sendCommand({
                                                'Id':cartridgeId
                                            },{
                                                "Event": "LOAD_AREA",
                                                "Session": {
                                                    "Id": sessionid
                                                },
                                                "Area": {
                                                    "Id": itm
                                                }
                                            });
                                        })
                                    } else {
                                        client.sendCommand({
                                            'Id':cartridgeId
                                        },{
                                            "Event": "LOAD_SESSION",
                                            "Session": {
                                                Id: sessionid
                                            }
                                        }).then(function () {
                                            console.log('ALL AREAS LOADED');
                                        });    
                                    }
                                    $('#actionSessionModal').modal('hide');
                                });
                                $('#actionSessionModal').modal('show');
                                break;
                        }
                    });

                    $('.activation-cb').unbind('change').change(function (evt) {
                        var sessionid = $(evt.currentTarget).data('sessionId');
                        var row_to_update = $(evt.currentTarget).closest('tr');
                        var grid = $('#sessions-grid').data('JSGrid');
                        var updated_sessions;
                        if($(evt.currentTarget).parent().find('input:checked').length > 0) {
                            updated_sessions = _.map(pgstate.get('sessions'), function (itm, idx) {
                                if(itm.Id === sessionid) {
                                    return _.extend({}, itm, {
                                        IsActive: true
                                    });
                                } else {
                                    return itm;
                                }
                            });
                            pgstate.set('sessions', updated_sessions);
                            client.sendCommand({
                                'Id':cartridgeId
                            },{
                                "Event": "ACTIVATE_SESSION",
                                "Session": {
                                    Id: sessionid
                                }
                            }).then(function (response) {
                                console.log('SESSION: ' + sessionid + ' ACTIVATED', response);
                                grid.updateItem(row_to_update, response.data);
                                initGridClicks();
                            });

                        } else {
                            updated_sessions = _.map(pgstate.get('sessions'), function (itm, idx) {
                                if(itm.Id === sessionid) {
                                    return _.extend({}, itm, {
                                        IsActive: false
                                    });
                                } else {
                                    return itm;
                                }
                            });
                            pgstate.set('sessions', updated_sessions);        
                            client.sendCommand({
                                'Id':cartridgeId
                            },{
                                "Event": "DEACTIVATE_SESSION",
                                "Session": {
                                    Id: sessionid
                                }
                            }).then(function (response) {
                                console.log('SESSION: ' + sessionid + ' DEACTIVATED', response);
                                grid.updateItem(row_to_update, response.data);
                                initGridClicks();
                            });
                        }
                    });

                }

                //RENDERER: SESSIONS PAGE
                function renderSessionsPage (context, response) {
                    context.partial('templates/sessions.template', {
                        items: response.data
                    }, function (context) {
                        var container_height = $('#sessions-grid').closest('.pg-sessions-grid-ct').height();
                        var title_height = $('#sessions-grid').prev().height();
                        var grid_width = $('#sessions-grid').width();
                        pgstate.set('sessions', response.data);
                        
                        $('#sessions-grid').jsGrid({
                            width: "100%",
                            height: container_height - title_height - 40,                     
                            inserting: false,
                            editing: false,
                            sorting: true,
                            paging: false,
                            data: pgstate.get('sessions'),
                            fields: [
                                { 
                                    title: "Session", 
                                    name: "Name", 
                                    width: grid_width * .35,
                                    type: "text" 
                                },
                                { 
                                    title: "Date", 
                                    name: "Date", 
                                    width: grid_width * .1, 
                                    type: "text" 
                                },
                                { 
                                    title: "Company", 
                                    name: "Company", 
                                    width: grid_width * .2, 
                                    type: "text" 
                                },
                                { 
                                    title: "Active", 
                                    type: "activetoggle", 
                                    width: grid_width * .1,
                                    name: "IsActive"
                                },
                                { 
                                    title: "Actions", 
                                    type: "actionslist", 
                                    width: grid_width * .25,
                                    sorting: false
                                }
                            ],
                            onRefreshed: function (args) {
                                initGridClicks();
                            },
                            confirmDeleting: false
                        });

                        _.defer(function () {
                            //fancy scroller
                            $('.jsgrid-grid-body').mCustomScrollbar({
                                theme: '3d-thick',
                                alwaysShowScrollbar: 1
                            });
                            $('#sessions-grid').data('JSGrid').sort({
                                field: 'Name',
                                order: 'asc'
                            });
                            initGridClicks();
                        });

                    });
                }

                //Preload slides for all sessions
                function RenderSessions (context, response) {                    
                    var all_slides = _.pluck(_.flatten(_.pluck(_.flatten(_.pluck(response.data, 'Areas')), 'Slides')), 'Thumbnail');
                    var all_refs = _.map(all_slides, function (itm, idx) {
                        var regex = /https?:\/\/\S+\/XmanagerWeb\/Xynco\/flipbooks\/(\S+)\//;
                        var m = regex.exec(itm);
                        return m ? m[1] : '';
                    });
                    var unique_assets = _.compact(_.uniq(all_refs));
                    var poll_flipbooks = _.map(unique_assets, function (itm, idx) {
                        return $.get("/XManagerWeb/Xynco/handlers/flipbook.ashx?method=info&id=" + itm);
                    });

                    $.when(poll_flipbooks).then(function () {
                        renderSessionsPage(context, response);
                    });
                }

                //ROUTE: Sessions List
                //DEST: #/sessions
                this.get('#/sessions', function (context) {
                    if(!pgstate.exists('current_user')) {
                        this.redirect('#/');
                    }
                    x2o.api.epoxy.get().then(function(epoxy){
                        client = epoxy;
                        client.connect(undefined, 'controller').then(function () {
                            if(pgstate.exists('selected_session')) {
                                pgtempstate.clearAll();
                                pgstate.clear('area_viewed');
                                pgstate.clear('disablesorting');
                                client.sendCommand({
                                    "Id": cartridgeId
                                }, {
                                    "Event": "GET_ALL_SESSIONS"
                                }).then(function (response) {
                                    if(response.success) {
                                        RenderSessions(context, response);
                                    } else {
                                        context.partial('templates/error.template', {
                                            item: {
                                                message: 'Get ALL Sessions Failed.'
                                            }
                                        });
                                    }
                                });
                            } else {
                                client.registerClient(cartridgeId, epoxy_events_handler);
                                client.sendCommand({
                                    "Id": cartridgeId
                                },{
                                    "Event": "REGISTER",
                                    "Data": {
                                        "Type": "Administrator",
                                        "NetworkId": getNetworkID()
                                    }
                                }).then(function (response) {
                                    if(response.success) {
                                        client.sendCommand({
                                            "Id": cartridgeId
                                        }, {
                                            "Event": "GET_ALL_SESSIONS"
                                        }).then(function (response) {
                                            if(response.success) {

                                                var all_sessions_response = response;

                                                //get current network layout
                                                client.sendCommand({
                                                    "Id": cartridgeId
                                                }, {
                                                    "Event": "GET_LAYOUT",
                                                    "Data": {
                                                        "Type": "Administrator",
                                                        "NetworkId": getNetworkID()
                                                    }
                                                }).then(function (response) {
                                                    if(response.success) {
                                                        //set session current network layout
                                                        pgstate.set('current_network_layout', response.data);
                                                        var available_areas = _.map(listAllAreasFromLayout(pgstate.get('current_network_layout')), function (itm, idx) {
                                                            return _.extend({
                                                                CurrentIndex: 0,
                                                                Id: "",
                                                                Name: "",
                                                                NetworkId: getNetworkID(),
                                                                Screens: [],
                                                                Slides: [],
                                                                State: ""
                                                            }, itm);
                                                        });
                                                        //available areas - has area info and order
                                                        pgstate.set('available_areas', available_areas);

                                                        // RenderSessions(context, response);
                                                        RenderSessions(context, all_sessions_response);

                                                    } else {
                                                        context.partial('templates/error.template', {
                                                            item: {
                                                                message: 'Get Layout Failed.'
                                                            }
                                                        });
                                                    }
                                                });

                                            } else {
                                                context.partial('templates/error.template', {
                                                    item: {
                                                        message: 'Get ALL Sessions Failed.'
                                                    }
                                                });
                                            }
                                        });
                                    } else {
                                        context.partial('templates/error.template', {
                                            item: {
                                                message: 'Registration Failed.'
                                            }
                                        });
                                    }
                                });
                            }
                            context.log("Epoxy client connected!");
                        })
                        .fail(function (err) {
                            context.log("Connection to server failed: " + err);
                            context.partial('templates/login.template', {
                                item: {
                                    message: "Connection to server failed: " + err
                                }
                            });
                        });
                    });
                });

                function checkSaveDetails () {
                    $('.pg-session-ct #details-panel input').each(function (idx, itm) {
                        switch ($(itm).attr('name')) {
                            case 'sessionname':
                                if($(itm).val() !== pgstate.get('selected_session').Name) {
                                    pgtempstate.set('Name', $(itm).val());
                                } else {
                                    pgtempstate.clear('Name');
                                }
                                break;
                            case 'date':
                                if($(itm).val() !== pgstate.get('selected_session').Date) {
                                    pgtempstate.set('Date', $(itm).val());
                                } else {
                                    pgtempstate.clear('Date');
                                }
                                break;
                            case 'company':
                                if($(itm).val() !== pgstate.get('selected_session').Company) {
                                    pgtempstate.set('Company', $(itm).val());
                                } else {
                                    pgtempstate.clear('Company');
                                }
                                break;
                        }
                    });
                }

                //INIT: session creator/editor page
                function initSessionEditorPage (context, action) {
                    //fix height of .pg-session-content-ct
                    var container_height = $('#container').outerHeight();
                    var navbar_height = $('#container nav').outerHeight() + 15;
                    $('.pg-session-ct').outerHeight(container_height - navbar_height);

                    //init details panel state based on set details data
                    var all_metafields_set = 
                        (pgstate.get('selected_session').Name || $('.pg-session-ct #details-panel input[name="sessionname"]').val()) && 
                        (pgstate.get('selected_session').Date || $('.pg-session-ct #details-panel input[name="date"]').val())&& 
                        (pgstate.get('selected_session').Company || $('.pg-session-ct #details-panel input[name="company"]').val());

                    //fix height of session details editor panel
                    var main_header_ct_height = $('.main-header-ct').outerHeight();
                    var session_name_field_height = $('#session-name').outerHeight();
                    var content_height = container_height - navbar_height - main_header_ct_height;
                    
                    //fix total height of details panel
                    var heading_one_height = $('#headingOne').outerHeight();
                    var heading_two_height = $('#headingTwo').outerHeight();
                    var collapse_one_height = $('#collapseOne').outerHeight();
                    $('#details-panel').outerHeight(content_height);

                    //set height of area selector panel
                    var area_list_height = all_metafields_set ? 
                    content_height - heading_one_height - session_name_field_height - heading_two_height - 40 :
                    content_height - heading_one_height - session_name_field_height - heading_two_height - collapse_one_height - 60;
                    $('.area-list-ct .card-body').outerHeight(area_list_height);

                    // area scrollbar
                    $('.area-list-ct .card-body').mCustomScrollbar({
                        theme: '3d-thick'
                    });
                    
                    //init grid
                    var slides_inventory = pgstate.get('area_viewed').Slides;
                    $('#sortable').sortable({
                        forcePlaceholderSize: true,
                        forceHelperSize: true,
                        update: function (evt, ui) {
                            var list = _.map($('#sortable li h4.slide-number'), function (itm) {
                                var ref_index = parseInt($(itm).data('positionIndex'));
                                return slides_inventory[ref_index];
                            });
                            var areas = orderAreaList(_.map(pgstate.get('valid_selected_areas'), function (itm, idx) {
                                if(itm.Id === pgstate.get('area_viewed').Id) {
                                    return injectAreaDetails({
                                        Id: pgstate.get('area_viewed').Id,
                                        Slides: list
                                    });
                                } else {
                                    return injectAreaDetails(itm);
                                }
                            }));

                            checkSaveDetails();

                            pgtempstate.set('Areas', areas);
                            pgstate.set('area_viewed', _.findWhere(areas, {
                                Id: pgstate.get('area_viewed').Id
                            }));
                            app.refresh();
                        }
                    });

                    //disable card content selection
                    $('#sortable').disableSelection();
                    
                    //sortable enabled check
                    if(!pgstate.exists('disablesorting')) {
                        $('#sortable').sortable('enable');
                    } else {
                        $('#sortable').sortable('disable');
                    }

                    //slide fancy scroller
                    $('.slide-ct').mCustomScrollbar({
                        theme: '3d-thick'
                    });

                    if(pgstate.exists('scrollto_request')) {
                        $('.slide-ct').mCustomScrollbar("scrollTo", 'h4[data-position-index="' + pgstate.get('scrollto_request') + '"]', {
                            scrollEasing: 'easeInOut'
                        });
                        pgstate.clear('scrollto_request');
                    }

                    //frame details editor
                    $('.pg-configure-frame-details').click(function (evt) {
                        var positionIndex = parseInt($(evt.currentTarget).parent().find('h4').first().data('positionIndex'));
                        // var targetSlide = pgstate.get('area_viewed').Slides[positionIndex];
                        var targetSlide = slides_inventory[positionIndex];
                        
                        //populate notes
                        var regex = /0,(.+),0/gm,
                        m = regex.exec(targetSlide.SpeakerNotes);
                        $('#detailsEditorModal').modal('show');
                        if(m) {
                            $('#detailsEditorModal textarea').val(m[1]);
                        } else {
                            $('#editor-collapseOne textarea').val(targetSlide.SpeakerNotes);
                        }
                        //populate AV array
                        var screens = _.map(pgstate.get('area_viewed').Children, function (itm, idx) {
                            return _.extend({}, itm, _.findWhere(pgstate.get('all_screens'), {
                                Id: itm.Id
                            }))
                        });
                        var available_av_commands = _.flatten(_.pluck(screens, 'AVCommands'));
                        var selected_av_commands = _.filter(targetSlide.AVCommands, function (itm, idx) {
                            return _.contains(_.pluck(available_av_commands, 'Name'), itm.Name);
                        });

                        var av_commands_list = _.reduce(available_av_commands, function (acc, itm, idx) {
                            var out = 
                                '<label class="pg-custom-checkbox custom-control custom-checkbox d-flex align-items-center justify-content-start">'
                                + '<input type="checkbox" class="custom-control-input" data-area-id="AV-' + idx + '"'
                                + (_.contains(_.pluck(selected_av_commands, 'Name'), itm.Name) ? ' checked' : '') + '>'
                                + '<span class="custom-control-indicator mr-4"></span>'
                                + '<span class="custom-control-description frutiger-light font-16">' + itm.Name + '</span>'
                                + '</label>';
                            return acc + out;
                        }, '');

                        //reset details editor display to notes
                        $('#editor-collapseOne').addClass('show');
                        $('#editor-collapseTwo').removeClass('show');
                        //push avc to list display panel
                        $('#editor-collapseTwo .card-body').html(av_commands_list);
                        //set save method to modal save button
                        $('#frame-editor-save').unbind('click').click(function (evt) {
                            //compile changes to session
                            var updated_notes = $('#editor-collapseOne textarea').val();
                            var updated_AVCommands = _.map($('#editor-collapseTwo input:checked'), function (itm, idx) {
                                var command_name = $(itm).next().next().text();
                                var out = _.findWhere(available_av_commands, {
                                    Name: command_name
                                });
                                return out;
                            });
                            var updated = _.extend({}, targetSlide, {
                                SpeakerNotes: updated_notes,
                                AVCommands: updated_AVCommands
                            });
                            slides_inventory[positionIndex] = updated;

                            checkSaveDetails();
        
                            var updated_slides = _.map($('#sortable li h4.slide-number'), function (itm) {
                                var ref_index = parseInt($(itm).data('positionIndex'));
                                return slides_inventory[ref_index];
                            });

                            var areas = orderAreaList(_.map(pgstate.get('valid_selected_areas'), function (itm, idx) {
                                if(itm.Id === pgstate.get('area_viewed').Id) {
                                   return injectAreaDetails({
                                       Id: pgstate.get('area_viewed').Id,
                                       Slides: updated_slides
                                   });
                                } else {
                                    return injectAreaDetails(itm);
                                }
                            }));

                            pgtempstate.set('Areas', areas);
                            pgstate.set('area_viewed', _.findWhere(areas, {
                                Id: pgstate.get('area_viewed').Id
                            }));

                            //save enable check
                            if(pgtempstate.keys().length > 0) {
                                $('#save-content').prop('disabled', false);
                            } else {
                                $('#save-content').prop('disabled', true);
                            }

                            //update indicators
                            var target_slide_card = $('h4[data-position-index="'+ positionIndex +'"]').parent();
                            if(updated.SpeakerNotes.length > 0) {
                                $(target_slide_card).find('.notes-icon').removeClass('d-none');
                            } else {
                                $(target_slide_card).find('.notes-icon').addClass('d-none');
                            }
                            if(updated.AVCommands.length > 0) {
                                $(target_slide_card).find('.light-icon').removeClass('d-none');
                            } else {
                                $(target_slide_card).find('.light-icon').addClass('d-none');
                            }

                            //hide modal
                            $('#detailsEditorModal').modal('hide');
                        });
                    });

                    //sorting lock
                    $('#iseditable').change(function (evt) {
                        if($(evt.target).parent().find('input:checked').length > 0) {
                            $('#sortable').sortable('enable');
                            pgstate.clear('disablesorting');
                            $('#iseditable-text').text('Unlocked');
                        } else {
                            $('#sortable').sortable('disable');
                            pgstate.set('disablesorting', true);
                            $('#iseditable-text').text('Locked');
                        }
                    });

                    //Details fields
                    $('.pg-session-ct #details-panel input').change(function (evt) {
                        switch ($(evt.currentTarget).attr('name')) {
                            case 'sessionname':
                                if($(evt.currentTarget).val() !== pgstate.get('selected_session').Name) {
                                    pgtempstate.set('Name', $(evt.currentTarget).val());
                                } else {
                                    pgtempstate.clear('Name');
                                }
                                break;
                            case 'date':
                                if($(evt.currentTarget).val() !== pgstate.get('selected_session').Date) {
                                    pgtempstate.set('Date', $(evt.currentTarget).val());
                                } else {
                                    pgtempstate.clear('Date');
                                }
                                break;
                            case 'company':
                                if($(evt.currentTarget).val() !== pgstate.get('selected_session').Company) {
                                    pgtempstate.set('Company', $(evt.currentTarget).val());
                                } else {
                                    pgtempstate.clear('Company');
                                }
                                break;
                        }
                        if(pgtempstate.keys().length > 0) {
                            $('#save-content').prop('disabled', false);
                        } else {
                            $('#save-content').prop('disabled', true);
                        }
                    });

                    //accordian behavior
                    $('.collapse').on('shown.bs.collapse', function (evt) {
                        var area_list_height = content_height - heading_one_height - session_name_field_height - heading_two_height - collapse_one_height - 40;
                        $('.area-list-ct .card-body').outerHeight(area_list_height);    
                        $('.area-list-ct .card-body').mCustomScrollbar('update');
                    }).on('hidden.bs.collapse', function (evt) {
                        var area_list_height = content_height - heading_one_height - session_name_field_height - heading_two_height - 40;    
                        $('.area-list-ct .card-body').outerHeight(area_list_height);
                        $('.area-list-ct .card-body').mCustomScrollbar('update');
                    });

                    //open area panel if area_viewed populated
                    if(!all_metafields_set) {
                        $('#collapseOne').collapse('show');
                    }

                    //update area_view
                    $('.area-list-ct span').click(function (evt) {
                        var areaid = $(evt.currentTarget).data('areaId');
                        var session_selected = _.findWhere(pgstate.get('selected_areas'), {
                            Id: areaid
                        });
                        var available_selected = _.findWhere(pgstate.get('available_areas'), {
                            Id: areaid
                        });
                        var area_viewed =  session_selected || available_selected;
                        area_viewed = _.extend({}, available_selected, area_viewed);
                        pgstate.set('area_viewed', area_viewed);
                        app.refresh();
                    });

                    //content updates (asset selector)
                    $('#select-content').click(function (evt) {
                        evt.preventDefault();
                        x2oAssetSelector.init({
                            FilterTypes: ['Powerpoint', 'Video', 'Image'],
                            MultiSelect: false,
                            AllowInheritedAssets: false,
                            IncludeRoot: false,
                            DefaultAssetCategory: "PowerPoint",
                            Height: 540,
                            OKButtonCallback: function(assetIds, assets) {
                                if(assets[0].Type === 'Powerpoint') {
                                    $.get(restAPIURL + "/assets/" + assetIds[0]).then(function(data){
                                        var metadata = {},
                                            thumbnailInfo = false,
                                            speakerNotes = false,
                                            assetId = false,
                                            metadataArray = data.content.Metadata;
                                        if(!metadataArray) return;
            
                                        metadataArray.forEach(function(val, idx){
                                            metadata[val.Label] = val.Value;
                                        });
                                        if( !metadata.ConversionStatus ||
                                            !metadata.HTML ||
                                            !metadata.SlideNotes ||
                                            !metadata.Thumbnails ||
                                            metadata.ConversionStatus !== 'Done'
                                        ) {
                                            return;
                                        }
                                        assetId = metadata.HTML;
                                        thumbnailInfo = $.get("/XManagerWeb/Xynco/handlers/flipbook.ashx?method=info&id=" + metadata.Thumbnails);
                                        speakerNotes = $.get(getAssetURL(metadata.SlideNotes));
    
                                        $.when(thumbnailInfo, speakerNotes).then(function(thumbnailResponse, speakerNotesResponse){
                                            var numberOfSlides = JSON.parse(thumbnailResponse[0]).FrameCount,
                                                speakerNotes = JSON.parse(speakerNotesResponse[0]),
                                                slides = [], areas;
                                            for( var i = 0; i < numberOfSlides; i++ ) {
                                                var slide = {
                                                    Asset: assetId,
                                                    AssetType: "ppt",
                                                    Index: i,
                                                    SpeakerNotes: speakerNotes[i].reduce(function(newVal, val) {
                                                        return (newVal += val);
                                                    }, ""),
                                                    Thumbnail: getBaseURL() + "XmanagerWeb/Xynco/flipbooks/" + metadata.Thumbnails + "/" + ("0000" + i).slice(-4) + ".png",
                                                    AVCommands: []
                                                };
                                                slides.push(slide);
                                            }
                                            //- if valid
                                            if(_.contains(_.pluck(pgstate.get('valid_selected_areas'), 'Id'), pgstate.get('area_viewed').Id)) {
                                                //- if already selected -> update area in selected_areas
                                                var areas = orderAreaList(_.map(pgstate.get('valid_selected_areas'), function (itm, idx) {
                                                    if(itm.Id === pgstate.get('area_viewed').Id) {
                                                       return injectAreaDetails({
                                                           Id: pgstate.get('area_viewed').Id,
                                                           Slides: itm.Slides.concat(slides)
                                                       });
                                                    } else {
                                                        return injectAreaDetails(itm);
                                                    }
                                                }));
                                            } else {
                                                //- else add new element area to end
                                                areas = orderAreaList(pgstate.get('valid_selected_areas').concat(injectAreaDetails({
                                                    Id: pgstate.get('area_viewed').Id,
                                                    Slides: slides
                                                })));
                                            }
                                            pgtempstate.set('Areas', areas);
                                            pgstate.set('area_viewed', _.findWhere(areas, {
                                                Id: pgstate.get('area_viewed').Id
                                            }));
                                            app.refresh();
                                        });
                                    });    

                                } else {
                                    var video_asset = assets[0];
                                    var video_slide = {
                                        Asset: video_asset.Id,
                                        AssetType: video_asset.Type.toLowerCase(),
                                        Index: 0,
                                        SpeakerNotes: "",
                                        Thumbnail: getBaseURL() + "XManagerWeb/REST/v1/Assets/" + video_asset.Id + "/thumbnail",
                                        AVCommands: []
                                    }
                                    //- if valid
                                    if(_.contains(_.pluck(pgstate.get('valid_selected_areas'), 'Id'), pgstate.get('area_viewed').Id)) {
                                        //- if already selected -> update area in selected_areas
                                        var areas = orderAreaList(_.map(pgstate.get('valid_selected_areas'), function (itm, idx) {
                                            if(itm.Id === pgstate.get('area_viewed').Id) {

                                                return injectAreaDetails({
                                                    Id: pgstate.get('area_viewed').Id,
                                                    Slides: itm.Slides.concat(video_slide)
                                                });

                                            } else {
                                                return injectAreaDetails(itm);
                                            }
                                        }));
                                    } else {
                                        //- else add new element area to end
                                        areas = orderAreaList(pgstate.get('valid_selected_areas').concat(injectAreaDetails({
                                            Id: pgstate.get('area_viewed').Id,
                                            Slides: [video_slide]
                                        })));
                                    }
                                    pgtempstate.set('Areas', areas);
                                    pgstate.set('area_viewed', _.findWhere(areas, {
                                        Id: pgstate.get('area_viewed').Id
                                    }));
                                    app.refresh();
                                }
                            }
                        });
                        x2oAssetSelector.show();
                    });

                    $('#remove-content').click(function (evt) {
                        evt.preventDefault();
                        //set modal content
                        $('#actionSessionModalContent').text('Remove content from Area \'' + pgstate.get('area_viewed').Name + '\' ?');
                        //set modal title
                        $('#actionSessionModalLabel').text('Remove Area Content');
                        //set modal button label
                        $('#session-action').text('Delete');
                        //set modal button click action
                        //save confirm
                        $('#session-action').unbind('click').click(function (evt) {
                            evt.preventDefault();
                            var areas = orderAreaList(_.compact(_.map(pgstate.get('valid_selected_areas'), function (itm, idx) {
                                if(itm.Id === pgstate.get('area_viewed').Id) {
                                    return;
                                } else {
                                    return injectAreaDetails(itm);
                                }
                            })));
                            pgtempstate.set('Areas', areas);
                            pgstate.set('area_viewed', _.findWhere(areas, {
                                Id: pgstate.get('area_viewed').Id
                            }));
                            app.refresh();
                            $('#actionSessionModal').modal('hide');
                        });
                        $('#actionSessionModal').modal('show');
                    });

                    $('.pg-item-remove').click(function (evt) {
                        evt.preventDefault();
                        var positionIndex = $(evt.currentTarget).closest('.slide-ct li.card').index();
                        var updated_slides = _.filter(pgstate.get('area_viewed').Slides, function (itm, idx) {
                            return idx !== positionIndex
                        });
                        var areas = orderAreaList(_.map(pgstate.get('valid_selected_areas'), function (itm, idx) {
                            if(itm.Id === pgstate.get('area_viewed').Id) {
                                return injectAreaDetails({
                                    Id: pgstate.get('area_viewed').Id,
                                    Slides: updated_slides
                                });
                            } else {
                                return injectAreaDetails(itm);
                            }
                        }));
                        pgtempstate.set('Areas', areas);
                        pgstate.set('area_viewed', _.findWhere(areas, {
                            Id: pgstate.get('area_viewed').Id
                        }));

                        checkSaveDetails();

                        //delete DOM element for removed slide
                        $(evt.currentTarget).closest('.slide-ct li.card').remove();

                        //check save state
                        if(pgtempstate.keys().length > 0) {
                            $('#save-content').prop('disabled', false);
                        } else {
                            $('#save-content').prop('disabled', true);
                        }

                    });

                    //save button enable
                    if(pgtempstate.keys().length > 0) {
                        $('#save-content').prop('disabled', false);
                    } else {
                        $('#save-content').prop('disabled', true);
                    }

                    //save button click
                    $('#save-content').click(function (evt) {
                        var check = !(pgtempstate.get('Name') || pgstate.get('selected_session').Name)
                            || !(pgtempstate.get('Date') || pgstate.get('selected_session').Date)
                            || !(pgtempstate.get('Company') || pgstate.get('selected_session').Company);

                        if(check) {
                            $('#requiredFieldsModalContent').text('Session Name, Date, and Company are required to be filled in to save this session.  Please review your session details.');
                            $('#requiredFieldsModal').modal('show');
                            $('#collapseOne').collapse('show');
                            return;
                        }

                        //set modal content
                        $('#actionSessionModalContent').text('Save session \'' + (pgtempstate.get('Name') || pgstate.get('selected_session').Name) + '\' ?');
                        //set modal title
                        $('#actionSessionModalLabel').text('Save session');
                        //set modal button label
                        $('#session-action').text('Save');
                        //set modal button click action
                        //save confirm
                        if (action === 'create') {
                            $('#session-action').unbind('click').click(function (evt) {
                                var updated_session = _.extend({}, pgstate.get('selected_session'), function () {
                                    var out = _.reduce(pgtempstate.keys(), function (acc, itm, idx) {
                                        var updated = _.extend({}, acc);
                                        updated[itm] = pgtempstate.get(itm);
                                        return updated;
                                    }, {});
                                    return out;
                                }());
                                client.sendCommand({
                                    'Id':cartridgeId
                                },{
                                    "Event": "CREATE_SESSION",
                                    "Session": updated_session
                                }).then(function () {
                                    $('#actionSessionModal').modal('hide');
                                });
                                app.trigger('close.session');    
                            });
    
                        } else {
                            $('#session-action').unbind('click').click(function (evt) {
                                var updated_session = _.extend({}, pgstate.get('selected_session'), function () {
                                    var out = _.reduce(pgtempstate.keys(), function (acc, itm, idx) {
                                        var updated = _.extend({}, acc);
                                        updated[itm] = pgtempstate.get(itm);
                                        return updated;
                                    }, {});
                                    return out;
                                }());
                                client.sendCommand({
                                    'Id':cartridgeId
                                },{
                                    "Event": "MODIFY_SESSION",
                                    "Session": updated_session
                                }).then(function () {
                                    $('#actionSessionModal').modal('hide');
                                });
                                app.trigger('close.session');    
                            });
                        }
                        $('#actionSessionModal').modal('show');   
                    });
                }

                //RENDERER: SESSION EDITOR PAGE
                function renderSessionEditorPage (context) {

                    //areas that have been saved in the viewed session - accounts for saved instances of areas during session edits
                    var selected_areas = pgtempstate.exists('Areas') ? pgtempstate.get('Areas') : pgstate.get('selected_session').Areas;
                    pgstate.set('selected_areas', selected_areas);

                    //session saved areas that are in the current network layout
                    //inject area names/details
                    var valid_selected_areas = _.map(_.filter(selected_areas, function (itm, idx) {
                        return isValidArea(itm);
                    }), function (itm, idx) {
                        return injectAreaDetails(itm);
                    });
                    pgstate.set('valid_selected_areas', valid_selected_areas);

                    // area_viewed
                    var area_viewed;
                    if(pgstate.exists('area_viewed')) {
                        //set if set in memory
                        area_viewed = pgstate.get('area_viewed');
                    } else {
                        //set to default
                        area_viewed = _.first(pgstate.get('valid_selected_areas')) || _.first(pgstate.get('available_areas'));
                        pgstate.set('area_viewed', area_viewed);
                    }
        
                    context.partial('templates/session.template', {
                        session: _.extend({}, pgstate.get('selected_session'), {
                            Name: (pgtempstate.exists('Name') ? pgtempstate.get('Name') : pgstate.get('selected_session').Name) || '',
                            Date: pgtempstate.exists('Date') ? pgtempstate.get('Date') : pgstate.get('selected_session').Date,
                            Company: (pgtempstate.exists('Company') ? pgtempstate.get('Company') : pgstate.get('selected_session').Company) || ''
                        }),
                        areas: _.map(pgstate.get('available_areas'), function (itm, idx) {
                            return _.extend({}, itm, {
                                isSelected: isSelectedArea(itm)
                            }, getAreaData(itm));
                        }),
                        area_viewed: area_viewed,
                        slides: area_viewed.Slides,
                        sortable: !pgstate.exists('disablesorting'),
                        action: 'edit'
                    }, function (context) {
                        initSessionEditorPage(context);
                    });
                }

                //ROUTE session editor
                //DEST: #/session/edit/:sessionid
                this.get('#/session/edit/:sessionid', function (context) {
                    //get current session
                    var selected_session = _.findWhere(pgstate.get('sessions'), {
                        Id: this.params['sessionid']
                    });
                    pgstate.set('selected_session', selected_session);

                    //get all areas
                    if(!pgstate.exists('all_screens')) {
                            client.sendCommand({
                            "Id": cartridgeId
                        }, {
                            "Event": "GET_ALL_SCREENS"
                        }).then(function (response) {

                            if(response.success) {
                                pgstate.set('all_screens', response.data);
                                renderSessionEditorPage(context);    
                            } else {
                                context.partial('templates/error.template', {
                                    item: {
                                        message: 'Get ALL Screens Failed.'
                                    }
                                });
                            }
                        });
                    } else {
                        renderSessionEditorPage(context);
                    }

                });

                //RENDERER: SESSION CREATOR PAGE
                function renderSessionCreatorPage (context) {

                    //available areas
                    //areas that have been saved in the viewed session
                    var selected_areas = pgtempstate.exists('Areas') ? pgtempstate.get('Areas') : pgstate.get('selected_session').Areas;
                    // session saved state takes into account previous saves in the session
                    pgstate.set('selected_areas', selected_areas); 

                    //saved areas that are in the current network layout
                    var valid_selected_areas = _.map(_.filter(selected_areas, function (itm, idx) {
                        return isValidArea(itm);
                    }), function (itm, idx) {
                        return injectAreaDetails(itm);
                    });

                    // selected session areas that are in the system
                    pgstate.set('valid_selected_areas', valid_selected_areas); 

                    //initial area viewed - first on list of valid areas if none set
                    var area_viewed;
                    if(pgstate.exists('area_viewed')) {
                        area_viewed = pgstate.get('area_viewed');
                    } else {
                        area_viewed = _.first(pgstate.get('valid_selected_areas')) || _.first(pgstate.get('available_areas'));
                        pgstate.set('area_viewed', area_viewed);
                    }
                    pgstate.set('area_viewed', area_viewed);

                    context.partial('templates/session.template', {
                        session: _.extend({}, pgstate.get('selected_session'), {
                            Name: (pgtempstate.exists('Name') ? pgtempstate.get('Name') : pgstate.get('selected_session').Name) || '',
                            Date: pgtempstate.exists('Date') ? pgtempstate.get('Date') : pgstate.get('selected_session').Date,
                            Company: (pgtempstate.exists('Company') ? pgtempstate.get('Company') : pgstate.get('selected_session').Company) || ''
                        }),
                        areas: _.map(pgstate.get('available_areas'), function (itm, idx) {
                            return _.extend({}, itm, {
                                isSelected: isSelectedArea(itm)
                            }, getAreaData(itm));
                        }),
                        area_viewed: area_viewed,
                        slides: area_viewed.Slides,
                        sortable: !pgstate.exists('disablesorting'),
                        action: 'create'
                    }, function (context) {
                        initSessionEditorPage(context, 'create');
                    });
                }

                //ROUTE create new session 
                // DEST: #/session/create
                this.get('#/session/create', function (context) {
                    //get current session
                    pgstate.set('selected_session', {});
                    //get current network layout
                    client.sendCommand({
                        "Id": cartridgeId
                    }, {
                        "Event": "GET_LAYOUT",
                        "Data": {
                            "Type": "Administrator",
                            "NetworkId": getNetworkID()
                        }
                    }).then(function (response) {
                        if(response.success) {

                            //set session current network layout
                            pgstate.set('current_network_layout', response.data);

                            //get all areas
                            if(!pgstate.exists('all_screens')) {
                                    client.sendCommand({
                                    "Id": cartridgeId
                                }, {
                                    "Event": "GET_ALL_SCREENS"
                                }).then(function (response) {

                                    if(response.success) {
                                        pgstate.set('all_screens', response.data);
                                        renderSessionCreatorPage(context);    
                                    } else {
                                        context.partial('templates/error.template', {
                                            item: {
                                                message: 'Get ALL Screens Failed.'
                                            }
                                        });
                                    }
                                });
                            } else {
                                renderSessionCreatorPage(context);
                            }
                        } else {
                            context.partial('templates/error.template', {
                                item: {
                                    message: 'Get Layout Failed.'
                                }
                            });
                        }
                    });
                });

                //EVENT: LOG IN FROM SPLASH
                //TRIGGER: log.in
                this.bind('log.in', function () {
                    this.redirect('#/sessions');
                });

                //EVENT: LOG OUT FROM SESSION MANAGER
                //TRIGGER: log.out
                this.bind('log.out', function () {
                    client.sendCommand({
                        "Id": cartridgeId
                    },{
                        "Event": "UNREGISTER"
                    }).then(function (response) {
                        console.log(response);
                    });

                    $('#logoutModal').modal('hide');
                    pgstate.clearAll();
                    pgcookie.clearAll();
                    pgtempstate.clearAll();

                    //LOGOUT X2O
                    this.redirect('#/');
                });

                //EVENT: CLOSE EDITING/CREATING SESSION
                //TRIGGER: close.session
                this.bind('close.session', function () {
                    this.redirect('#/sessions');
                });

                //EVENT: EDIT SELECTED SESSION
                //TRIGGER: edit.session
                this.bind('edit.session', function (evt, dat) {
                    this.redirect('#/session/edit/' + dat.sessionid);
                });
            });

            $(function () {
                app.run('#/');
            });

        });
    });
})();