<div class="pg-session-ct d-flex flex-column mt-4">
    <div class="main-header-ct row flex-shrink-0 flex-grow-0 mt-0">
        <div class="main-header col-md-10 offset-md-1 d-flex flex-row align-items-center justify-content-between">
            <div class="d-flex flex-row align-items-center">
                <h1 class="text-left text-uppercase frutiger-light font-26 pt-4 mb-4 mr-4"><%= action.charAt(0).toUpperCase() + action.substr(1) %> session</h1>    
                <a id="session-cancel" role="button" class="nav-link frutiger-light font-16" href="#/sessions">
                    <i class="fas fa-backspace"></i> Cancel
                </a>
            </div>

            <button id="save-content" type="button" class="btn btn-primary btn-rounded-small-pg" disabled>Save session</button>        
        </div>
    </div>
    <div class="pg-session-content-ct row">
        <div class="card col-md-3 offset-md-1">
            <div class="card-block d-flex flex-column">
                <h1 class="text-left text-uppercase frutiger-bold font-24 flex-shrink-0 flex-grow-0">Session details</h1>
                <form class="flex-shrink-0 flex-grow-0" action="#/session/update/details/<%= session.Id %>" method="post">
                    <div class="form-group">
                        <label for="sessionname" class="frutiger-light font-16">Session name</label>
                        <input type="text" class="form-control frutiger-light font-16" name="sessionname" minlength="2" placeholder="SessionXYZ ..." value="<%= session.Name %>">
                    </div>
        
                    <div class="form-group">
                        <label for="date" class="frutiger-light font-16">Date</label>
                        <input type="date" class="form-control frutiger-light font-16" name="date" minlength="2" placeholder="2018-12-31 ..." value="<%= session.Date %>">
                    </div>
        
                    <div class="form-group">
                        <label for="company" class="frutiger-light font-16">Company</label>
                        <input type="text" class="form-control frutiger-light font-16" name="company" minlength="2" placeholder="CompanyXYZ ..." value="<%= session.Company %>">
                    </div>        
                </form>
                <div class="pg-area-list-header flex-shrink-0 flex-grow-0 d-flex flex-row justify-content-between align-items-center mb-4">
                    <h1 class="text-left text-uppercase frutiger-bold font-24 my-0">Session Areas</h1>
                </div>
                <div class="area-list-ct d-flex flex-column mb-4" style="flex: 1; overflow: auto;">
                    <% for (i=0; i < areas.length ; i++) { %>
                    <label class="pg-custom-checkbox custom-control custom-checkbox d-flex align-items-center justify-content-start">
                        <input type="checkbox" class="custom-control-input" data-area-id="<%= areas[i].Id %>"<% if (areas[i].isSelected) { %> checked<% } %>>
                        <span class="custom-control-indicator mr-4"></span>
                        <span class="custom-control-description frutiger-light font-16"><%= areas[i].Name %></span>
                    </label>
                    <% } %>
                </div>
            </div>
        </div>
        
        <div class="card col-md-7 d-flex flex-column">
            <div class="pg-content-editor-header d-flex align-items-center justify-content-between my-4">
                <h1 class="text-left text-uppercase frutiger-bold font-26">Session area content</h1>
                <div class="pg-editable d-flex align-items-center justify-content-between">
                    <span class="leader-btn-label frutiger-roman font-16">Unlock</span>
                    <label class="switch">
                        <input id="iseditable" type="checkbox"<% if(sortable) { %> checked<% } %>>
                        <span class="slider round"></span>
                    </label>
                </div>
                <button id="select-content" type="button" class="btn btn-primary btn-rounded-xsmall-pg">Select content</button>
            </div>
            <div class="dropdown pg-dropdown d-flex align-items-center justify-content-end" id="areaselect">
                <button class="btn btn-secondary btn-block dropdown-toggle frutiger-roman font-22 m-0 d-flex align-items-center justify-content-between" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span><%= areaviewed.Name ? areaviewed.Name : 'No areas assigned to session' %></span><span></span>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <% for (var i = 0; i < areasrest.length; i++ ) { %>
                        <button class="dropdown-item frutiger-roman font-22" data-area-id="<%= areasrest[i].Id %>"><%= areasrest[i].Name %></button>
                    <% } %>    
                </div>
            </div>
            <div class="slide-ct">
                <div class="inner-slide-ct d-flex justify-content-center">
                    <ul id="sortable" class="pg-slide-display-cards ui-sortable overthrow d-flex flex-wrap justify-content-start">
                        <% for (var i = 0; i < slides.length; i++) { %>
                        <li class="card card-inverse ui-state-default ui-sortable-handle d-flex align-items-center">
                            <img class="card-img" src="<%= slides[i].Thumbnail %>" alt="slide-<%= slides[i].Asset + '__' + slides[i].Index %>">
                            <div class="card-img-overlay p-0">
                                <h4 class="slide-number frutiger-roman font-22" data-slide-index="<%=slides[i].Asset + '__' + slides[i].Index %>"><%= i + 1 %></h4>
                                <img src="./images/notes-icon@2x.png" class="notes-icon<% if (slides[i].SpeakerNotes.length === 0) { %> d-none<% } %>">
                                <img src="./images/light-changes-icon@2x.png" class="light-icon<% if (slides[i].AVCommands.length === 0) { %> d-none<% } %>">
                                <button class="pg-configure-frame-details">
                                    <i class="fas fa-wrench"></i>    
                                </button>                
                            </div>
                        </li>
                        <% } %>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
