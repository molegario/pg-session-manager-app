<div class="pg-session-ct d-flex flex-column mt-4">
    <div class="main-header-ct row flex-shrink-0 flex-grow-0 mt-0">
        <div class="main-header col-md-10 offset-md-1 d-flex flex-row align-items-center justify-content-between">
            <div class="d-flex flex-row align-items-center">
                <h1 class="text-left text-uppercase frutiger-light font-26 pt-4 mb-4 mr-4"><%= action.charAt(0).toUpperCase() + action.substr(1) %> session</h1>    
                <a id="session-cancel" role="button" class="nav-link text-uppercase frutiger-light font-16" href="#/sessions">
                    <i class="fas fa-backspace"></i> Cancel
                </a>
            </div>
            <button id="save-content" type="button" class="btn btn-primary btn-rounded-small-pg" disabled>Save session</button>        
        </div>
    </div>
    <div class="pg-session-content-ct row">
        <div class="card col-md-3 offset-md-1">
            <div id="accordion" class="">
                <div id="details-panel" class="card border-0 d-flex">

                    <div class="card-header pg-details-item-header pt-4" id="headingOne" style="flex: 0;">
                        <h1 class="mb-0 text-left text-uppercase frutiger-bold font-24">
                            Session details
                            <button class="btn btn-link text-uppercase frutiger-light font-16" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <i class="fas fa-edit"></i> Edit
                            </button>
                        </h1>
                    </div>

                    <div id="session-name" class="form-group" style="flex: 0;">
                        <label for="sessionname" class="frutiger-light font-16">Session name</label>
                        <input type="text" class="form-control frutiger-light font-16" name="sessionname" minlength="2" placeholder="SessionXYZ ..." value="<%= session.Name %>">
                    </div>

                    <div id="collapseOne" class="collapse hide" aria-labelledby="headingOne" style="flex-shrink: 1;">
                        <div class="card-body frutiger-roman font-16">
                            <div class="form-group">
                                <label for="date" class="frutiger-light font-16">Date</label>
                                <input type="date" class="form-control frutiger-light font-16" name="date" minlength="2" placeholder="2018-12-31 ..." value="<%= session.Date %>">
                            </div>                
                            <div class="form-group">
                                <label for="company" class="frutiger-light font-16">Company</label>
                                <input type="text" class="form-control frutiger-light font-16" name="company" minlength="2" placeholder="CompanyXYZ ..." value="<%= session.Company %>">
                            </div>
                        </div>
                    </div>

                    <div class="card-header pg-details-item-header pt-4 mb-4" id="headingTwo" style="flex:0;">
                        <h1 class="mb-0 text-left text-uppercase frutiger-bold font-24">
                            Session Areas
                        </h1>
                    </div>

                    <div id="collapseTwo" class="area-list-ct" aria-labelledby="headingTwo" style="flex-grow:1;">
                        <div class="card-body frutiger-roman font-16">
                            <% for (i=0; i < areas.length ; i++) { %>
                            <label class="pg-custom-checkbox custom-control custom-checkbox d-flex align-items-center justify-content-start">
                                <span class="custom-control-description frutiger-light font-16<%= areas[i].isSelected ? ' pg-itm-has-content' : '' %>" data-area-id="<%= areas[i].Id %>">
                                    <%= areas[i].Name %> <% if(areas[i].Slides && areas[i].Slides.length) {%>( <%= areas[i].Slides.length %> )<% } %>
                                </span>
                            </label>
                            <% } %>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="card col-md-7 d-flex flex-column">
            <div class="pg-content-editor-header d-flex align-items-center justify-content-between my-4">
                <h1 class="text-left text-uppercase frutiger-bold font-26"><%= area_viewed.Name %></h1>
                <div class="pg-editable d-flex align-items-center justify-content-between">
                    <span id="iseditable-text" class="leader-btn-label frutiger-roman font-16"><%= sortable ? 'Unlocked' : 'Locked' %></span>
                    <label class="switch">
                        <input id="iseditable" type="checkbox"<% if(sortable) { %> checked<% } %>>
                        <span class="slider round"></span>
                    </label>
                </div>
                <button id="remove-content" type="button" class="btn btn-danger btn-rounded-xsmall-pg">Remove all content</button>
                <button id="select-content" type="button" class="btn btn-primary btn-rounded-xsmall-pg">Select content</button>
            </div>
            <div class="slide-ct">
                <div class="inner-slide-ct d-flex justify-content-center">
                    <ul id="sortable" class="pg-slide-display-cards ui-sortable overthrow d-flex flex-wrap justify-content-start">
                        <% for (var i = 0; i < slides.length; i++) { %>
                        <li class="card card-inverse ui-state-default ui-sortable-handle d-flex align-items-center">
                            <img class="card-img" src="<%= slides[i].Thumbnail %>" alt="slide-<%= slides[i].Asset + '__' + slides[i].Index %>">
                            <div class="card-img-overlay p-0">
                                <h4 class="slide-number frutiger-roman font-22" data-position-index="<%= i %>" data-slide-index="<%=slides[i].Asset + '__' + slides[i].Index %>"><%= i + 1 %></h4>
                                <img src="./images/notes-icon@2x.png" class="notes-icon<% if (slides[i].SpeakerNotes.length === 0) { %> d-none<% } %>">
                                <img src="./images/light-changes-icon@2x.png" class="light-icon<% if (slides[i].AVCommands.length === 0) { %> d-none<% } %>">
                                <button class="pg-configure-frame-details">
                                    <i class="fas fa-wrench"></i>    
                                </button>                
                                <button class="pg-item-remove">
                                    <i class="fa fa-times"></i>    
                                </button>                
                            </div>
                        </li>
                        <% } %>
                    </ul>
                </div>
            </div>
        </div>    
    </div>
</div>
